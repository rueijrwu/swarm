#pragma once
#include <Eigen/Eigen>
#include <array>

class GLobject {
public:
    GLobject();

    virtual ~GLobject() { shutdown(); }

    virtual void render();

    void setColor(float r, float g, float b);

    void setColor(float* direction);

    void setColor(const std::array<float, 3> &color);

    void setColor(const Eigen::Array3f &color);

    void setScale(float scale);

    virtual void setup();

    virtual void update(const Eigen::Vector3f &pos,
                        const Eigen::Vector3f &direction,
                        const Eigen::Vector3f &up,
                        const Eigen::Vector3f &right);

    void setPosition(const Eigen::Vector3f &pos);

protected:

    void updateUniform();

    virtual void setVertices(float* vertices, uint size);

    virtual void setVertices(const std::vector<float> &vertices);



    void shutdown();

    void updateColor();

    void updateVerticies();

    Eigen::Array3f _color;
    uint _ibo;
    std::vector<uint> _indices;
    Eigen::Matrix4f _model;
    Eigen::Vector3f _position;
    float _scale;
    uint _vao;
    uint _vbo;
    std::vector<float> _vertices;
};