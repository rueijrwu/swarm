#pragma once
#include <algorithm>
#include <memory>

#include "swarm_definition.hpp"
#include "swarm_object.hpp"
#include "elman_net.hpp"
#include "gl_boid.hpp"

#include "food.hpp"
#include "signal.hpp"

/**
 * Class of Boid agent.
 *
 */
class Boid {
public:
    /**
     * Constructor.
     *
     * @param generation the generation of Boid agent
     * @param energy the energy of Boid agent
     */
    Boid(const float& generation,
         const float& energy,
         const vec3f& pos,
         const float& range,
         uint64_t time=0);

    /**
     * Destructor.
     */
    ~Boid() {}

    /**
     * Copy operator.
     */
    Boid(const Boid &obj);
    /**
     * Assignment operator.
     */
    Boid& operator=(const Boid &obj);

    /**
     * Add to the energy of Boid agent.
     *
     * @param e the energy add to the energy of Boid agent
     */
    void addEnergy(const float &e)
    { _energy += e; }

    /**
     * Check boundary
     */
    void checkBoundary();

    /**
     * Get the energy of Boid agent.
     *
     * @retrun the energy of Boid agent
     */
    float energy() const
    { return _energy; }

    /**
     * Get the energy of Boid agent.
     * @retrun the energy of Boid agent
     */
    uint generation() const
    { return _generation; }

    /**
     * Initialization of Boid agent.
     *
     * @param cnt_hidden the size of hidden node
     * @param cnt_input the size of input node
     * @param cnt_output the size of output node
     * @param generation the generation of Boid agent
     * @param energy the energy of Boid agent
     */
    void init(const float& generation,
              const float& energy,
              const vec3f& pos,
              const float& range,
              uint64_t time);

    /**
     * Do feedforward
     */
    void feedForward()
    {
        _brian.feedForward();
    }

    /**
     * Do feedforward
     */
    void feedForward(const vec3f &signal)
    { _brian.feedForward(signal); }

    /**
     * Get the output of Boid brain.
     *
     * @param idx the index of the output of Boid brain
     */
    float getOutput(const int &idx) const
    { return _brian._output[idx]; }

    /**
     * Get the signal of Boid agent.
     *
     * @retrun the position of Boid agent
     */
    float getSignal() const
    { return _signal; }

    /**
     * Get the signal of Boid agent.
     *
     * @retrun the position of Boid agent
     */
    float getSignal(uint64_t time=0) const
    {
        if (time <= _time) return _prev_signal;
        else return _signal;
    }

    /**
     * Create new Boid agent from this agent.
     *
     * @param mutate_probability the probability of mutation
     * @retrun new Boid agent
     */
    BoidPtr giveBirth(const float& mutate_probability, float birth_rand);

    /**
     * Mutate.
     *
     * @param range the size of hidden node
     */
    void mutate(const float &range)
    { _brian.mutate(range); }

    const vec3f& position()
    { return _position; }


    const vec3f& position(uint64_t time) {
        if (time < _time) return _prev_position;
        else return _position;
    }

    /**
     * Get the right direction of Boid agent.
     *
     * @retrun the right direction of Boid agent
     */
    const vec3f& right() const
    { return _right; }

    /**
     * Render.
     */
    void render() {


        // TODO: Scale
        _gl.update(_position, _velocity, _right, _up);
        _gl.render();
    }

    /**
     * Set the energy of Boid agent.
     *
     * @param e the energy of Boid agent
     */
    void setEnergy(const float &e)
    { _energy = e; }


    void setPosition(const vec3f pos) {
        _position = pos;
        _prev_position = pos;
    }

    /**
     * Set the input of Boid brain.
     *
     * @param idx the index of the input of Boid brain
     * @param input the input of Boid brain
     */
    void setInput(const int &idx, const float &input)
    { _brian._input[idx] = input; }

    void setInput(const Eigen::Matrix<float, BOID_NUM_INPUT, 1> &input)
    { _brian._input = input; };

    /**
     * Set the up direction of Boid agent.
     */
    void setRight()
    { _right = _velocity.cross(_up); }

    /**
     * Set the velocity of Boid agent.
     */
    void setVelocity(const vec3f& vel)
    { _velocity = vel; }

    /**
     * Set the up direction of Boid agent.
     */
    void setUp(const vec3f& up)
    { _up = up; }

    /**
     * Get the velocity of Boid agent.
     *
     * @retrun the velocity of Boid agent
     */
    const vec3f& velocity() const
    { return _velocity; }

    /**
     * Get the up direction of Boid agent.
     *
     * @retrun the up direction of Boid agent
     */
    const vec3f& up() const
    { return _up; }

    void updatVelocityAndPosition();

private:
    ElmanNet _brian; /// the neuron network of agent
    float _energy;
    uint _generation;   /// the generation of agent
    GLboid  _gl;
    vec3f _position;
    vec3f _prev_position;
    float _prev_signal;
    vec3f _right;
    float _signal;
    uint64_t _time;
    vec3f _up;
    vec3f _velocity;    /// the velocity of agent
};
