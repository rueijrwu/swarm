#pragma once

#include <array>
#include <cstdio>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <Eigen/Eigen>

#define SKIP_FRAMES 5

#define UNIVERSE_SIZE 300
#define FOOD_AMOUNT 2.5f
#define FOOD_MAXIMUM_ENERGY .1f
#define FOOD_TOTAL_AMOUNT 10000.f
#define NET_WEIGHTS_AMPLITUDE 1.f

#define BOID_CHILD_SIZE 3
#define BOID_CHILD_BIRTH_RADIUS 20.f
#define BOID_INIT_ENERGY 2.f
#define BOID_MOVEMENT_COST 0.02f
#define BOID_NUM_INPUT 6
#define BOID_NUM_HIDDEN 10
#define BOID_NUM_OUTPUT 4
#define BOID_OUTPUT_COEF 0.0125f
#define BOID_MUTATE_PROBABILITY 0.1f
#define BOID_BIRTH_ENERGY_TRHESHOLD 6.f
#define BOID_BIRTH_ENERGY 2.f
#define BOID_DATH_ENERGY 1.f
#define BOID_NOISE_LEVEL 0.f
#define BOID_BRITH_PROBABILITY 0.5f

#define BOID_LARGE_POPULATION_COST_COEF 1.001f
#define BOID_SMALL_POPULATION_COST_COEF 1.01f

#define BOID_SIGNAL_COST_COEF 0.01f
#define BOID_SIGNAL_MIN_DISTANCE 1.f
#define BOID_SIGNAL_MAX_DISTANCE 100.f

#define BOID_ENERGY_COEF_POPULATION_THRESHOLD 200


using vec3f = Eigen::Vector3f;

class Boid;
using BoidPtr = std::shared_ptr<Boid>;

struct Params_t {
    Params_t() {
        is_signal_enabled = false;
        noise_level = 0.1f;
        signal_multiplier = 1.f;
        target_population = 200;
        num_boids = 100;
    }

    bool is_signal_enabled;
    float noise_level;
    float signal_multiplier;
    uint64_t target_population;
    uint num_boids;
};