#pragma once
#include "swarm_definition.hpp"
#include "swarm_object.hpp"

/**
 * Signal object.
 *
 */
class Signal {
public:
    /**
     * Constructor.
     */
    Signal();

    /**
     * Constructor.
     */
    Signal(const vec3f& v1,
           const vec3f& v2,
           const vec3f& v3);

    /**
     * Destructor.
     */
    ~Signal() {}

    /**
     * Acess operator for signal components.
     */
    float operator[](uint idx) const
    { return _components[idx]; }

    /**
     * Multiplication assignment operator for signal components by vec3f.
     *
     * @params n a vector to be multiplied into signal
     * @return this Signal object
     */
    Signal& operator+=(const vec3f &src);

    /**
     * Get axis of this Signal object.
     *
     * @return this axis of this signal object
     */
    const Eigen::Matrix3f& axis() const
    { return _transformation; }

//    /**
//     * Set axis of this Signal object.
//     *
//     * @params v1 the first vector of axis matrix
//     * @params v2 the second vector of axis matrix
//     * @params v3 the third vector of axis matrix
//     */
//    void setAxis(const vec3f& v1,
//                 const vec3f& v2,
//                 const vec3f& v3);

    void normalized()
    { _components.normalized(); }

    const vec3f& components()
    { return _components; }

    float value()
    { return _components.norm(); }

private:
    Eigen::Matrix3f _transformation;
    vec3f _components;
};
