#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

class GLFWengine {
public:
    GLFWengine() : _glfw_initialized(false)
    { setup(); }

    virtual ~GLFWengine()
    { shutdown(); }

    bool isInitialized()
    { return _glfw_initialized; }

    void setup() {
        if (_glfw_initialized) return;

        // Init glfw
        glfwInit();
        _glfw_initialized = true;
    };

    void shutdown() {
        if (!_glfw_initialized) return;

        glfwTerminate();
        _glfw_initialized = false;
    }

private:
    bool _glfw_initialized;
};

std::shared_ptr<GLFWengine> MakeGLFWengine();