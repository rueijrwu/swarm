#pragma once
#include "gl_object.hpp"

class GLboid : public GLobject {
public:
    GLboid();

    void render() override;

    uint getIBO()
    { return _ibo; }
};