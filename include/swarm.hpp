#pragma once
#include "swarm_definition.hpp"
#include "boid.hpp"
#include "food.hpp"
#include "gl_shpere.hpp"
#include "gl_world_cube.hpp"

/**
 * Class of Swarm
 *
 */
class Swarm {
public:
    /**
     * Constructor
     */
    Swarm() : _iteration(0) {}

    /**
     * Constructor
     */
    Swarm(const Params_t &params) : _params(params) {}

    /**
     * Destructor
     */
    ~Swarm() {}

    /**
     * Initialization of swarm.
     */
    void init();

    /**
     * Set parameters of swarm
     * @param params the set of parameters
     */
    void setParameters(const Params_t &params)
    { _params = params;}

    /**
     * Update swarm
     */
    void update();

private:
    std::vector<BoidPtr> _boids;
    Food _food;
    double _total_consumed_food;
    uint64_t _iteration;
    // std::string _name;
    Params_t _params;
    double _total_emitted_signal;

    GLsphere _gl_food;
    GLworldcube _gl_world;
};
