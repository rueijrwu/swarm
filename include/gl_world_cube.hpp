#pragma once
#include "gl_object.hpp"

class GLworldcube : public GLobject{
public:
    GLworldcube();
    ~GLworldcube() {}

    void render() override;
};
