#pragma once

const char* VERT_SHADER = R"(#version 430
layout (location = 0) in vec3 pos;

layout(location = 0) uniform mat4 projection_view;
layout(location = 1) uniform mat4 model;
layout(location = 2) uniform vec3 color;
layout(location = 3) uniform float scale;

void main() {
    gl_Position = projection_view * model * vec4(scale * pos.x, scale * pos.y, scale * pos.z, 1.f);
    //gl_Position = vec4(pos, 1.f);
}
)";

const char *FRAG_SHADER = R"(#version 430
layout(location = 0) uniform mat4 projection_view;
layout(location = 1) uniform mat4 model;
layout(location = 2) uniform vec3 color;
layout(location = 3) uniform float scale;

out vec4 out_color;

void main() {
    out_color = vec4(color, 1.f);
    //out_color = vec4(1.f, 0.f, 0.f, 1.f);
}
)";