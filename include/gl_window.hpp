#pragma once

#include <functional>
#include <memory>
#include <string>

#include "glfw_engine.hpp"
#include "gl_engine.hpp"
#include "swarm.hpp"

class GLwindow {
public:
    GLwindow(std::shared_ptr<GLFWengine> glfw_engine,
             std::string name="Swarm") {
        _glfw_engine = glfw_engine;
        _name = name;
        _win = nullptr;
    }

    GLwindow(const GLwindow &) = delete;

    ~GLwindow()
    { _gl_engine->shutdown(); }

    void connect(std::shared_ptr<Swarm> simulation)
    { _simulation = simulation; }

    void create(int width=800, int height=600);

    void resize(int width, int height);

    std::shared_ptr<GLengine> getGLengine()
    { return _gl_engine; }

    bool preRender();

    void swapBufferAndPullEvents();

private:

    std::shared_ptr<GLFWengine> _glfw_engine;
    std::shared_ptr<GLengine> _gl_engine;

    int _height;
    std::string _name;
    int _width;
    GLFWwindow* _win;

    std::shared_ptr<Swarm> _simulation;

};