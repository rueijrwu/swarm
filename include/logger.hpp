#pragma once

#include <memory>
#include <string>

#include "spdlog/spdlog.h"
#include "spdlog/async.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/rotating_file_sink.h"

/**
 * Logger class.
 *
 */
class Logger {
    using AsyncLogger = spdlog::async_logger;
    using AsyncLoggerPtr = std::shared_ptr<AsyncLogger>;
    using FileLogger = spdlog::sinks::rotating_file_sink_mt;
    using StdLogger = spdlog::sinks::stdout_color_sink_mt;
public:
    /**
     * Constructor
     */
    Logger() {
        auto stdout_sink = std::make_shared<StdLogger>();
        auto rotating_sink = std::make_shared<FileLogger>("log.txt", 1024 * 1024 * 10, 3);
        std::vector<spdlog::sink_ptr> sinks {stdout_sink, rotating_sink};
        auto logger = std::make_shared<AsyncLogger>(
                "swlog",
                sinks.begin(),
                sinks.end(),
                spdlog::thread_pool(),
                spdlog::async_overflow_policy::block);
    }
    /**
     * Destructor
     */
    ~Logger() {}

    /**
     * Debug level log.
     * @param msg the message to log
     */
    void debug(std::string msg)
    { _logger->debug(msg); }

    /**
     * Error level log.
     * @param msg the message to log
     */
    void error(std::string msg)
    { _logger->error(msg); }

    /**
     * Info level log.
     * @param msg the message to log
     */
    void info(std::string msg)
    { _logger->info(msg); }

    /**
     * Warnning level log
     * @param msg the message to log
     */
    void warn(std::string msg)
    { _logger->warn(msg); }

private:

    AsyncLoggerPtr _logger;
};