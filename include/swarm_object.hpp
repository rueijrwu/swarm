#pragma once

#include "swarm_definition.hpp"


/**
 * Basic class of swarm object
 *
 */
class SwarmObject {
public:
    SwarmObject() {
        _position.setZero();
    }

    virtual ~SwarmObject() {}


    virtual SwarmObject& operator=(const SwarmObject& obj) {
        _position = obj._position;
        return *this;
    }

    virtual SwarmObject& operator=(const vec3f& pos) {
        _position = pos;
        return *this;
    }

    /**
     * Get the position of swarm object.
     * @retrun the position of swarm object
     */
    const vec3f& position() const
    { return _position; }

    /**
     * Set the position of swarm object.
     * @param pos the position of swarm object
     */
    void setPosition(const Eigen::Vector3f& pos)
    { _position = pos; }

    /**
     * Set the position of swarm object.
     * @param px the x component of the position of swarm object
     * @param py the y component of the position of swarm object
     * @param pz the z component of the position of swarm object
     */
    void setPosition(const float &px, const float &py, const float &pz)
    { _position << px, py, pz; }

protected:
    vec3f _position;
};

