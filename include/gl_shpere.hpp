#pragma once
#include "gl_object.hpp"


class GLsphere : public GLobject {
public:
    GLsphere(uint sector=24, uint stack=12);
    ~GLsphere() {};

    void render() override;

protected:
    uint _sector;
    uint _stack;
};
