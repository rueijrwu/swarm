#pragma once
#include "swarm_definition.hpp"

inline vec3f arbitraryAxisRotation(const vec3f& velocity, const vec3f& axis, const float &theta) {

    float COS = std::cos(theta);
    float SIN = std::sin(theta);

    vec3f projection = (1 - COS) * (velocity.dot(axis)) * axis;
    vec3f vel_cos = velocity * COS;
    vec3f cross_sin = axis.cross(velocity) * SIN;

    return (projection + vel_cos + cross_sin).normalized();
}