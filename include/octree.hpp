#pragma once
#include <memory>

#include "swarm_definition.hpp"
#include "signal.hpp"
#include "boid.hpp"

/**
 * Octree class for signal collection and aggregation.
 *
 */
class Octree {
public:
    using OctreePtr = std::unique_ptr<Octree>;

private:
    using OctChild = std::map<uint8_t, OctreePtr>;

public:

    //Octree() : _origin(0, 0, 0), _size(UNIVERSE_SIZE / 2.f), _level(0) {}


    /**
     * Constructor.
     *
     * @param origin the origin of Octree node
     * @param halfDimension the half dimension of Octree node
     */
    Octree(const vec3f &origin, float size, int level) :
            _level(level),
            _origin(origin),
            _size(size) {}

    /**
     * Constructor.
     *
     * @param origin the origin of Octree node
     * @param halfDimension the half dimension of Octree node
     */
    Octree(const vec3f &origin, float size, int level, BoidPtr data) :
            _data(data),
            _level(level),
            _origin(origin),
            _size(size) {}
    /**
     * Copy operator.
     */
    Octree(const Octree& copy)
            : _size(copy._size),
              _level(copy._level),
              _origin(copy._origin) {}

    /**
    * Destructor.
    */
    ~Octree() {}

    /**
     * Collect signal of Octree children.
     *
     * @param signal the signal object to be collected
     * @param target_center the center of the target signal
     * @param signal_multiplier the multiplier of the signal
     */
    void collectSignal(vec3f &signal, const vec3f &target_center, uint64_t time) const;

    /**
     * Insert Boid into Octree.
     *
     * @param agent the Boid agent to be inseted into Octree
     */
    void insert(BoidPtr agent);

    /**
     * Update Octree.
     */
    void update();//int depth=0);

private:
    /**
     * Find which octant contains a point
     *
     * @param point a 3D point
     * @return the index of Octant
     */
    void getOctantFromPoint(const vec3f &point, vec3f& origin, uint8_t & idx) const;

    /**
     * Check Octree node is a leaf node.
     * @return true if the Octree node is a leaf node
     */
    bool isLeafNode() const
    { return (_children.size() == 0); }

    OctChild _children; /// Pointers to child octants

    BoidPtr _data;   /// Data point to be stored at a node

    int _level;

    vec3f _origin; /// The physical center of this node

    float _size;

    //static float SIGNAL_COEF;
};
