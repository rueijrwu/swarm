#pragma once
#include "swarm_definition.hpp"
#include "utility.hpp"
/**
 * Class of neuron network for each agent
 *
 */

//template<int BOID_NUM_INPUT, int BOID_NUM_HIDDEN, int BOID_NUM_OUTPUT>
class ElmanNet {
    friend Boid;
    using InputVec = Eigen::Matrix<float, BOID_NUM_INPUT, 1>;
    using HiddenVec = Eigen::Matrix<float, BOID_NUM_HIDDEN, 1>;
    using OutputVec = Eigen::Matrix<float, BOID_NUM_OUTPUT, 1>;
    using InputHiddenWeight = Eigen::Matrix<float, BOID_NUM_HIDDEN, BOID_NUM_INPUT>;
    using InputHiddenWeightArray = Eigen::Array<float, BOID_NUM_HIDDEN * BOID_NUM_INPUT, 1>;
    using HiddenOutputWeight = Eigen::Matrix<float, BOID_NUM_OUTPUT, BOID_NUM_HIDDEN>;
    using HiddenOutputWeightArray = Eigen::Array<float, BOID_NUM_OUTPUT * BOID_NUM_HIDDEN, 1>;


public:
    /**
     * Constructor.
     */
    ElmanNet() {};

    /**
     * Copy operator.
     */
    ElmanNet(const ElmanNet &obj) {
        _input = obj._input;
        _hidden = obj._hidden;
        _output = obj._output;
    };

    /**
     * Destructor.
     */
    ~ElmanNet() {};

    /**
     * Feed forward.
     */
    void feedForward() {
        _hidden = _input_to_hidden_weight * _input;
        _output = _hidden_to_output_weight * _hidden.unaryExpr(
                [](float element) -> float { return 1.f / (1.f + std::exp(-element)); });
    }

    /**
     * Feed forward.
     */
    void feedForward(const vec3f &input) {
        _input[0] = std::max(0.f, input[0]);
        _input[1] = std::max(0.f, -input[0]);
        _input[2] = std::max(0.f, input[1]);
        _input[3] = std::max(0.f, -input[1]);
        _input[4] = std::max(0.f, input[2]);
        _input[5] = std::max(0.f, -input[2]);
        feedForward();
    }

    /**
     * Initialization of swarm.
     */
    void init() {
        // Allocate memory of neurons, buffer and weight
        resetNeuron();
        _input_to_hidden_weight = InputHiddenWeight::Random() * NET_WEIGHTS_AMPLITUDE;
        _hidden_to_output_weight = (HiddenOutputWeight::Random().array() + 1.f) / 2.f * NET_WEIGHTS_AMPLITUDE;
    }

    /**
     * Mutate.
     *
     * @param range the size of
     */
    void mutate(const float &range) {
        {
            InputHiddenWeightArray coin = InputHiddenWeightArray::Random();
            InputHiddenWeightArray random_number = InputHiddenWeightArray::Random() * range;
            Eigen::Map<InputHiddenWeightArray> w(_input_to_hidden_weight.data(), BOID_NUM_HIDDEN * BOID_NUM_INPUT, 1);
            w = InputHiddenWeightArray::NullaryExpr(
                    [&w, &coin, &random_number](Eigen::Index i) {
                        if ((coin[i] + 1.f) / 2.f < BOID_MUTATE_PROBABILITY) {
                            return random_number[i];
                        } else {
                            return w[i];
                        }
                    }
            );
        }
        {
            HiddenOutputWeightArray coin = HiddenOutputWeightArray::Random();
            HiddenOutputWeightArray random_number = (HiddenOutputWeightArray::Random() + 1.f) / 2.f * range;
            Eigen::Map<HiddenOutputWeightArray> w(_hidden_to_output_weight.data(), BOID_NUM_OUTPUT * BOID_NUM_HIDDEN, 1);
            w = HiddenOutputWeightArray::NullaryExpr(
                    [&w, &coin, &random_number](Eigen::Index i) {
                        if ((coin[i] + 1.f) / 2.f < BOID_MUTATE_PROBABILITY) {
                            return random_number[i];
                        } else {
                            return w[i];
                        }
                    }
            );
        }
    }

    /**
     * Reset neuron.
     */
    void resetNeuron() {
        _input.setZero();
        _hidden.setZero();
        _output.setZero();
    }

private:

    InputVec _input;
    HiddenVec _hidden;
    OutputVec _output;
    InputHiddenWeight _input_to_hidden_weight;
    HiddenOutputWeight _hidden_to_output_weight;
};