#pragma once
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <gst/app/gstappsink.h>
#include <string>

class Streamer {
private:
    enum streamer_state_t {
        STREAMER_NOT_INIT = -1,
        STREAMER_IDLE = 0,
        STREAMER_RUNNING = 1,
        STREAMER_RUNNING_ALLOC = 2,
        STREAMER_RUNNING_PUSH = 3,
    };
public:
    Streamer() {
        _width = 800;
        _height = 600;
        _bytes = _width * _height * 3 * sizeof(unsigned char);
        _state = STREAMER_NOT_INIT;
        _file = "output.avi";
    }

    ~Streamer() {}

    void setFileName(std::string file) {
        _file = file;
        if (_sink != nullptr) {
            g_object_set(_sink, "location", _file.c_str(), nullptr);
        }
    }

    void setup();

    void setup(int width, int height, std::string filename);

    void shutdonw();

    void start();

    void stop();

    void push();

    void setSize(uint width, uint height);

    uint8_t* newFrame();

private:
    GstElement* _pipeline;
    GstElement* _source;
    GstElement* _converter;
    GstElement* _encoder;
    GstElement* _sink;
    GstBuffer* _frame;
    GstMapInfo _frame_map;

    std::string _file;
    uint _width;
    uint _height;
    uint _bytes;

    streamer_state_t _state;
};