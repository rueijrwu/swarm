#pragma once
#include <cmath>
#include "swarm_definition.hpp"
#include "swarm_object.hpp"


/**
 * Class of Food
 *
 */
class Food : public SwarmObject {
public:
    Food() {
        _amount = 0;
        _current_amount = 0;
        _range = 0;
        _total_amount = 0;
    }

    ~Food() {}

    void setup(float amount, float range, float total_amount) {
        _amount = amount;
        _current_amount = total_amount;
        _range = range;
        _total_amount = total_amount;
        _position = vec3f::Random() * _range;
    }

    /**
     * Get the amount of Food.
     * @retrun the amount of Food
     */
    float give(const vec3f pos)
    {
        float amount = std::min(_amount / (_position - pos).norm(), FOOD_MAXIMUM_ENERGY);
        if (_current_amount > amount) {
            _current_amount -= amount;
        } else if (_current_amount > 0) {
            amount =  _current_amount;
        } else {
            amount = 0;
        }
        _current_amount -= amount;
        return amount;
    }

    void update() {
        if (_current_amount <= 0) {
            _position = vec3f::Random() * _range;
            _current_amount = _total_amount;
        }
    }

private:
    float _amount;
    float _current_amount;
    float _total_amount;
    float _range;
};

