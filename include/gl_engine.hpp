#pragma once
#include <memory>
#include <glm/mat4x4.hpp>
#include "gl_object.hpp"
#include "streamer.hpp"

class GLengine {
public:
    GLengine() {
        _program = 0;
        _width = 0;
        _height = 0;
        _frameIdx = 0;
    }

    virtual ~GLengine() { shutdown(); }

    bool setup(int width, int height);

    uint getProgram()
    { return _program; }

    void preRender();

    void postRender();

    template<class T>
    std::shared_ptr<T> makeObject() {
        std::shared_ptr<T> obj = std::make_shared<T>();
        _objects.push_back(obj);
        return obj;
    }

    void resize(int width, int height);

    void setCamera();

    void shutdown();

//    void startSave()
//    { _streamer.start(); }
//
//    void stopSave()
//    { _streamer.stop(); }

private:
    void updateViewAndProjection();


    std::vector<std::shared_ptr<GLobject>> _objects;
    uint _program;
    glm::mat4 _projection_view;

    uint _width;
    uint _height;

    //Streamer _streamer; /// TODO: remove streamer out of gl

    uint64_t _frameIdx;
};

std::shared_ptr<GLengine> MakeGLengine();