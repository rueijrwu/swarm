#include <glad/glad.h>
#include <cmath>
#include "gl_shpere.hpp"

#include <iostream>

GLsphere::GLsphere(uint sector, uint stack) : GLobject() {
    _vertices.clear();
    _indices.clear();
    _sector = sector;
    _stack = stack;

    float sectorStep = 2.f * M_PI / _sector;
    float stackStep = M_PI / _stack;
    float sectorAngle;
    float stackAngle = M_PI / 2;
    for(int i = 0; i <= _stack; i++)
    {
        sectorAngle = 0;
        for(int j = 0; j <= _sector; j++)
        {
            _vertices.push_back(cosf(stackAngle) * cosf(sectorAngle));
            _vertices.push_back(cosf(stackAngle) * sinf(sectorAngle));
            _vertices.push_back(sinf(stackAngle));

            if ((j < _sector) && (i < _stack)) {
                int k1 = (_sector + 1) * i + j;
                int k2 = k1 + 1;
                int k3 = k2 + _sector;
                int k4 = k3 + 1;

                _indices.push_back(k1);
                _indices.push_back(k2);
                _indices.push_back(k3);

                _indices.push_back(k2);
                _indices.push_back(k3);
                _indices.push_back(k4);
            }
            sectorAngle += sectorStep;
        }
        stackAngle -= stackStep;
    }

}

void GLsphere::render() {
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBindVertexArray(_vao);
    updateUniform();
    glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, (GLvoid*)0);
    glBindVertexArray(0);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}