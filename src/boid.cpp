#include <cmath>

#include "boid.hpp"
#include "colormap.hpp"



Boid::Boid(const float& generation,
           const float& energy,
           const vec3f& pos,
           const float& range,
           uint64_t time) {
    init(generation, energy, pos, range, time);
}

Boid::Boid(const Boid &obj) {
    _brian = obj._brian;
    _generation = obj._generation;
    _energy = obj._energy;
    _position = obj._position;
    _prev_position = obj._prev_position;
    _prev_signal = obj._prev_signal;
    _right = obj._right;
    _signal = obj._signal;
    _time = obj._time;
    _up = obj._up;
    _velocity = obj._velocity;

}

Boid& Boid::operator=(const Boid &obj) {
    _brian = obj._brian;
    _generation = obj._generation;
    _energy = obj._energy;
    _position = obj._position;
    _prev_position = obj._prev_position;
    _prev_signal = obj._prev_signal;
    _right = obj._right;
    _signal = obj._signal;
    _time = obj._time;
    _up = obj._up;
    _velocity = obj._velocity;
    return *this;
}

void Boid::checkBoundary() {
    _position = _position.unaryExpr(
        [](float element)->float{
            if (element < -UNIVERSE_SIZE) element += 2 * UNIVERSE_SIZE;
            else if (element > UNIVERSE_SIZE) element -= 2 * UNIVERSE_SIZE;
            return element;
        }
    );
}

BoidPtr Boid::giveBirth(const float& mutate_probability, float birth_rand) {
    BoidPtr child;

    if (birth_rand > BOID_BRITH_PROBABILITY) {
        return child;
    }
    int generation = _generation + 1;
    child = std::make_shared<Boid>(
            generation,
            BOID_INIT_ENERGY,
            _position,
            BOID_CHILD_BIRTH_RADIUS / 2.f,
            _time + 1);

    child->_brian._input_to_hidden_weight = _brian._input_to_hidden_weight;
    child->_brian._hidden_to_output_weight = _brian._hidden_to_output_weight;
    child->mutate(mutate_probability);
    child->checkBoundary();

    uint8_t colorIdx = (generation % 16) * 16;
    child->_gl.setColor(COLORMAP[colorIdx]);
    return child;
}

void Boid::init(const float& generation,
                const float& energy,
                const vec3f& pos,
                const float& range,
                uint64_t time) {
    _generation = generation;
    _energy = energy;
    _position = pos + vec3f::Random() * range;
    _prev_position = _position;
    _prev_signal = 0;

    _velocity << vec3f::Random().normalized();
    _right << vec3f::Random();
    _right = (_right - _velocity.dot(_right) * _velocity).normalized();
    _up = _velocity.cross(_right);

    _signal = 0;
    _time = time;

    // Initialize brian
    _brian.init();

    _gl.setup();
}

void Boid::updatVelocityAndPosition() {
    // Update velocity and position
    Eigen::Array3f noise = BOID_NOISE_LEVEL * Eigen::Array3f::Random();
    float bias = 0.25 * BOID_NUM_HIDDEN;
    float psi = (_brian._output[0] + noise[0] - bias) * M_PI * BOID_OUTPUT_COEF;
    float theta = (_brian._output[1] + noise[1] - bias) * M_PI * BOID_OUTPUT_COEF;
    float phi = (_brian._output[2] + noise[2] - bias) * M_PI * BOID_OUTPUT_COEF;

    float COS_THETA = cos(theta);
    float SIN_THETA = sin(theta);
    float COS_PHI = cos(phi);
    float SIN_PHI = sin(phi);
    float COS_PSI = cos(psi);
    float SIN_PSI = sin(psi);
    Eigen::Matrix3f transformation;
    transformation << COS_THETA * COS_PSI, COS_THETA * SIN_PSI, -SIN_THETA,
                      -COS_PHI * SIN_PSI + SIN_PHI * SIN_THETA * COS_PSI, COS_PHI * COS_PSI + SIN_PHI * SIN_THETA * SIN_PSI, SIN_PHI * COS_THETA,
                      SIN_PHI * SIN_PSI + COS_PHI * SIN_THETA * COS_PSI, -SIN_PHI * COS_PSI + COS_PHI * SIN_THETA * SIN_PSI, COS_PHI * COS_THETA;

//    transformation << COS_THETA * COS_PSI, COS_THETA * SIN_PSI, -SIN_THETA,
//                      -SIN_PSI, COS_PSI, 0,
//                      SIN_THETA * COS_PSI, SIN_THETA * SIN_PSI, COS_THETA;


    _prev_position = _position;
    _prev_signal = _signal;

    _velocity = transformation * _velocity;
    _up = transformation * _up;
    _right = transformation * _right;

    _position += _velocity;
    checkBoundary();

    _signal = _brian._output[3] * BOID_OUTPUT_COEF;
};