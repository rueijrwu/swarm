#include <iostream>

#include "gl_window.hpp"
#include "gl_boid.hpp"


//glm::mat4 camera(float Translate, glm::vec2 const & Rotate)
//{
//    glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.f);
//    glm::mat4 View = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -Translate));
//    View = glm::rotate(View, Rotate.y, glm::vec3(-1.0f, 0.0f, 0.0f));
//    View = glm::rotate(View, Rotate.x, glm::vec3(0.0f, 1.0f, 0.0f));
//    glm::mat4 Model = glm::scale(glm::mat4(1.0f), glm::vec3(0.5f));
//    return Projection * View * Model;
//}

std::shared_ptr<GLFWengine> MakeGLFWengine() {
    return std::make_shared<GLFWengine>();
}

void GLwindow::create(int width, int height) {
    if (!_glfw_engine->isInitialized()) return;

    // Set all the required options for GLFW
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    _win = glfwCreateWindow(width, height, _name.c_str(), nullptr, nullptr);
    if (_win == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        return;
    }

    glfwMakeContextCurrent(_win);
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        return;
    }

    _gl_engine = MakeGLengine();

    // Set callback
    glfwSetWindowUserPointer(_win, this);
    glfwSetFramebufferSizeCallback(
            _win,
            [](GLFWwindow* win, int width, int height) {
                GLwindow* app = static_cast<GLwindow*>(glfwGetWindowUserPointer(win));
                app->resize(width, height);
            }
    );

    // Build shader
    if (!_gl_engine->setup(width, height)) {
        glfwDestroyWindow(_win);
        _win = nullptr;
        return;
    }

    _gl_engine->resize(width, height);

    auto obj = _gl_engine->makeObject<GLboid>();

    _width = width;
    _height = height;
}

void GLwindow::resize(int width, int height)
{
    if (_win == nullptr) return;
    _gl_engine->resize(width, height);
}

bool GLwindow::preRender() {
    // No window
    if (_win == nullptr) {
        return false;
    }

    // Exit window
    if (glfwWindowShouldClose(_win)) {
        glfwDestroyWindow(_win);
        return false;
    }

    _gl_engine->preRender();
    return true;
}

void GLwindow::swapBufferAndPullEvents() {

    _gl_engine->postRender();

    /* Swap front and back buffers */
    glfwSwapBuffers(_win);

    /* Poll for and process events */
    glfwPollEvents();
}