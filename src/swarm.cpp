#include "swarm.hpp"
#include "octree.hpp"
#include "signal.hpp"
#include "utility.hpp"

void Swarm::init() {
    _iteration = 0;

    // Initialize Boid agent
    for (int i=0; i<_params.num_boids; i++) {
        auto boid = std::make_shared<Boid>(0, BOID_INIT_ENERGY, vec3f(0, 0, 0), UNIVERSE_SIZE / 2.);
        _boids.push_back(boid);
    }

//    for (int i =0;i<10;i++) {
//        auto boid = std::make_shared<Boid>(0, BOID_INIT_ENERGY, UNIVERSE_SIZE / 10.);
//        boid->feedForward();
//        _boids.push_back(boid);
//    }
//    auto boid = std::make_shared<Boid>(0, BOID_INIT_ENERGY, BOID_INIT_ENERGY, UNIVERSE_SIZE / 20.);
//    _boids.push_back(boid);

    _food.setup(FOOD_AMOUNT, UNIVERSE_SIZE / 4, FOOD_TOTAL_AMOUNT);

    _gl_food.setColor(1, 0, 0);
    _gl_food.setScale(5.f);
    _gl_food.setup();

    _gl_world.setScale(UNIVERSE_SIZE);
    _gl_world.setup();

    //_food.setPosition(Eigen::Array<float, 3, 1>::Random() * UNIVERSE_SIZE);
}

void Swarm::update() {
    // Adjust constant
//    float ENERGY_COST_COEF = (_boids.size() > BOID_ENERGY_COEF_POPULATION_THRESHOLD ?
//                              BOID_SMALL_POPULATION_COST_COEF : BOID_LARGE_POPULATION_COST_COEF);

    float ENERGY_COST_COEF = 1.f;
    int population = _boids.size();

    // Create Octree
    Octree::OctreePtr octree = std::make_unique<Octree>(vec3f(0 ,0, 0), UNIVERSE_SIZE / 2.f, 0);
    if (_params.is_signal_enabled) {
        for (auto &boid:_boids) {
            octree->insert(boid);
        }
    }


    //if (population < _params.target_population) {
        // Update Boid
        std::vector<BoidPtr> childs;
        Eigen::ArrayXf random_number;
        random_number.setRandom(_boids.size() * BOID_CHILD_SIZE);
        random_number = (random_number + 1.f) / 2.f;
        int idx = 0;
        for (std::vector<BoidPtr>::iterator it = _boids.begin();it != _boids.end();) {
            Boid* boid = it->get();

            // Check the energy of each boid agent
            for (int k=0; k<BOID_CHILD_SIZE; k++)
            {
                if (boid->energy() < BOID_BIRTH_ENERGY_TRHESHOLD) break;
                BoidPtr child = boid->giveBirth(NET_WEIGHTS_AMPLITUDE, random_number(BOID_CHILD_SIZE * idx + k));
                if (child) {
                    childs.push_back(child);
                    boid->addEnergy(-BOID_BIRTH_ENERGY);
                }
            }

            if (boid->energy() < BOID_DATH_ENERGY || boid->generation() > 5000) {
                it = _boids.erase(it);
                continue;
            }

            // Collect signal and do forward feed
            float signal_cost_coef = 0;
            vec3f signal(0.f, 0.f, 0.f);
            if (_params.is_signal_enabled) {
                octree->collectSignal(signal, boid->position(), _iteration);
                signal_cost_coef = BOID_SIGNAL_COST_COEF; //TODO: mulitply signal?
            }
            signal = _params.signal_multiplier * signal;// + _params.noise_level * vec3f::Random();

            //Update input from signal
            boid->feedForward(signal);

            // Update energy
            float food_energy = _food.give(boid->position());
            boid->addEnergy(food_energy - ENERGY_COST_COEF * (signal_cost_coef * signal.norm() + BOID_MOVEMENT_COST));

            // Update velocity and position
            boid->updatVelocityAndPosition();

            boid->render();
            it++;
            idx++;
        }

        if (childs.size() > 0) {
            _boids.insert(_boids.end(), childs.begin(), childs.end());
        }

//    } else {
//        for (std::vector<BoidPtr>::iterator it = _boids.begin();it != _boids.end();) {
//            Boid* boid = it->get();
//
//            // Check the energy of each boid agent
//            if (boid->energy() < BOID_DATH_ENERGY || boid->generation() > 5000) {
//                it = _boids.erase(it);
//                continue;
//            }
//
//            // Collect signal and do forward feed
//            float signal_cost_coef = 0;
//            vec3f signal(0.f, 0.f, 0.f);
//            if (_params.is_signal_enabled) {
//                octree->collectSignal(signal, boid->position(), _iteration);
//                signal_cost_coef = BOID_SIGNAL_COST_COEF; //TODO: mulitply signal?
//            }
//            signal = _params.signal_multiplier * signal;// + _params.noise_level * vec3f::Random();
//
//            //Update input from signal
//            boid->feedForward(signal);
//
//            // Update energy
//            float food_energy = _food.give(boid->position());
//            boid->addEnergy(food_energy - ENERGY_COST_COEF * (signal_cost_coef * signal.norm() + BOID_MOVEMENT_COST));
//
//            boid->updatVelocityAndPosition();
//            boid->render();
//            it++;
//        }
//    }

    _food.update();
    _gl_food.setPosition(_food.position());


    _gl_food.render();
    _gl_world.render();

    _iteration++;
    std::printf("Time:\t%lu\tSize:\t%lu\n", _iteration, _boids.size());
}