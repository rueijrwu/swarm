//#include <algorithm>
#include <iostream>
#include <glad/glad.h>
#include "gl_object.hpp"

GLobject::GLobject() {
    _color << 1.f, 1.f, 1.f;
    _ibo = 0;
    _model = Eigen::Matrix4f::Identity();
    _scale = 1.f;
    _vao = 0;
    _vbo = 0;
    _vertices = {
            -1.f,  1.f,  1.f, // Front-top-left
            1.f,  1.f,  1.f, // Front-top-right
            -1.f, -1.f,  1.f, // Front-bottom-left
            1.f, -1.f,  1.f, // Front-bottom-right
            1.f, -1.f, -1.f, // Back-bottom-right
            1.f,  1.f,  1.f, // Front-top-right
            1.f,  1.f, -1.f, // Back-top-right
            -1.f,  1.f,  1.f, // Front-top-left
            -1.f,  1.f, -1.f, // Back-top-left
            -1.f, -1.f,  1.f, // Front-bottom-left
            -1.f, -1.f, -1.f, // Back-bottom-left
            1.f, -1.f, -1.f, // Back-bottom-right
            -1.f,  1.f, -1.f, // Back-top-left
            1.f,  1.f, -1.f  // Back-top-right
    };
}

void GLobject::updateUniform() {
    glUniformMatrix4fv(1, 1, GL_FALSE,_model.data());
    glUniform3fv(2, 1, _color.data());
    glUniform1f(3, _scale);
}

void GLobject::render() {
    glBindVertexArray(_vao);
    updateUniform();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);
    glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, (GLvoid*)0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void GLobject::setColor(float r, float g, float b) {
    _color << r, g, b;
}

void GLobject::setColor(float* color) {
    _color << color[0], color[1], color[2];
}

void GLobject::setColor(const std::array<float, 3> &color) {
    _color << color[0], color[1], color[2];
}

void GLobject::setColor(const Eigen::Array3f &color) {
    _color = color;
}

void GLobject::setScale(float scale) {
    _scale = scale;
}

void GLobject::setPosition(const Eigen::Vector3f& pos) {
    _position = pos;
    _model(0, 3) = pos[0];
    _model(1, 3) = pos[1];
    _model(2, 3) = pos[2];
}

void GLobject::setup() {
    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);
    glGenBuffers(1, &_ibo);

    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(
            GL_ARRAY_BUFFER,
            _vertices.size() * sizeof(float),
            _vertices.data(),
            GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(uint), _indices.data(), GL_DYNAMIC_DRAW);

    updateUniform();

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
            0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*) 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
;
}

void GLobject::setVertices(float* vertices, uint size) {
    _vertices.clear();
    std::copy(vertices, vertices + size, std::back_inserter(_vertices));
    updateVerticies();
}

void GLobject::setVertices(const std::vector<float> &vertices) {
    _vertices.clear();
    std::copy(vertices.begin(), vertices.end(), std::back_inserter(_vertices));
    updateVerticies();
}

void GLobject::shutdown() {
    if (_ibo) {
        glDeleteBuffers(1, &_ibo);
    }

    if (_vbo) {
        glDeleteBuffers(1, &_vbo);
    }

    if (_vao) {
        glDeleteBuffers(1, &_vao);
    }
}

void GLobject::update(const Eigen::Vector3f &pos,
                      const Eigen::Vector3f &direction,
                      const Eigen::Vector3f &up,
                      const Eigen::Vector3f &right) {

    Eigen::Matrix4f model;
    _model << Eigen::Vector4f(direction.x(), direction.y(), direction.z(), 0),
              Eigen::Vector4f(right.x(), right.y(), right.z(), 0),
              Eigen::Vector4f(up.x(), up.y(), up.z(), 0),
              Eigen::Vector4f(pos.x(), pos.y(), pos.z(), 1);;
}

void GLobject::updateVerticies() {
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(
        GL_ARRAY_BUFFER,
        _vertices.size() * sizeof(float),
        _vertices.data(),
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}