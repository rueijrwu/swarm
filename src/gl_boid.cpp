#include <glad/glad.h>
#include "gl_boid.hpp"
#include <iostream>

GLboid::GLboid() : GLobject() {
    _vertices = { 2.5f,  0.f,  0.f,
                 -2.5f, -1.f, -1.f,
                 -2.5f, -1.f,  1.f,
                 -2.5f,  1.f,  1.f,
                 -2.5f,  1.f, -1.f};
    _indices = {0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 1, 4, 1, 2, 4, 2, 3, 4};

    setColor(0, 0.5, 0.0625);
    setScale(1.5f);
}

void GLboid::render() {
    glBindVertexArray(_vao);
    updateUniform();
    glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, (GLvoid*)0);
    glBindVertexArray(0);
}
