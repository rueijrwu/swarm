#include "swarm.hpp"
#include <CLI/CLI.hpp>
#include <chrono>
#include <ctime>

#include "gl_window.hpp"
#include "gl_object.hpp"

int main(int argc, char * argv[])
{

    Params_t params;
    uint64_t cnt_iterations_max = 20000;

    // Processing options
    CLI::Option *noise_level_opt;
    CLI::Option *num_boids_opt;
    CLI::Option *target_population_opt;
    CLI::Option *signal_multiplier_opt;
    CLI::Option *iterations_max_opt;
    if (argc > 1) {
        CLI::App app("Swarm");
        noise_level_opt = app.add_option(
                "-l,--noise_level",
                params.noise_level,
                "Noise level");

        num_boids_opt = app.add_option(
                "-n,--number_boids",
                params.num_boids,
                "Number of Boids");

        target_population_opt = app.add_option(
                "-p,--target_population",
                params.target_population,
                "Population of target");

        signal_multiplier_opt = app.add_option(
                "-s,--signal_mulitplier",
                params.signal_multiplier,
                "Signal multiplier");

        iterations_max_opt = app.add_option(
                "-i,--max_iterations",
                cnt_iterations_max,
                "Maximum of iterations");
        CLI11_PARSE(app, argc, argv);
    }

    // Create GLFW engine
    auto glfw_engine = MakeGLFWengine();

    // Create Window
    GLwindow app(glfw_engine);
    app.create();
    auto gl_engine = app.getGLengine();

    // Create swarm simulation
    std::shared_ptr<Swarm> swarm = std::make_shared<Swarm>(params);
    swarm->init();
    app.connect(swarm);

    srand(time(nullptr));
    for (uint64_t i=0;i < cnt_iterations_max; i++)
    {
        if (!app.preRender()) break;

        swarm->update();
        app.swapBufferAndPullEvents();
    }

//    auto time_start = std::chrono::high_resolution_clock::now();
//    for (uint64_t i=0;i < cnt_iterations_max; i++)
//    {
//        swarm.update();
//        //if ((i % 200) == 0) std::cout << i <<"\n";
//    }
//
//    std::chrono::duration<double> time_elpase =
//            std::chrono::high_resolution_clock::now() - time_start;
//    std::cout<<"Done! \n";
//    std::cout<<"Time per iteration = " <<
//        time_elpase.count() / 1000.f / cnt_iterations_max << "\n";

    std::cout << "Simulation is terminated" << std::endl;

    return 0;
}