#include "octree.hpp"
//float Octree::SIGNAL_COEF = 1.f;


void Octree::getOctantFromPoint(const vec3f &point, vec3f& origin, uint8_t & idx) const {
    origin = _origin;
    idx = 0;

    if (point.x() >= _origin.x()) {
        idx |= 4;
        origin[0] += 0.5f * _size;
    } else {
        origin[0] -= 0.5f * _size;
    }

    if (point.y() >= _origin.y()) {
        idx |= 2;
        origin[1] += 0.5f * _size;
    } else {
        origin[1] -= 0.5f * _size;
    }


    if (point.z() >= _origin.z()) {
        idx |= 1;
        origin[2] += 0.5f * _size;
    } else {
        origin[2] -= 0.5f * _size;
    }
    return;
}

void Octree::collectSignal(vec3f &signal, const vec3f &target, uint64_t time) const
{
    vec3f origin_to_target = target - _origin;
    float R = _size + 100;
    if ((abs(origin_to_target.x()) < R) &&
        (abs(origin_to_target.y()) < R) &&
        (abs(origin_to_target.z()) < R) &&
        (origin_to_target.norm() < R)) {

        if (isLeafNode()) { // Leaf node
            vec3f to_target = target - _data->position(time);
            float d = to_target.norm();
            //float d2 = std::pow(to_target.norm(), 2);

            // If the position of signaling boid is too close or too far, skip this signal
            if ((d < BOID_SIGNAL_MIN_DISTANCE) ||  (d > BOID_SIGNAL_MAX_DISTANCE)) {
                return;
            }

            float signal_magnitude = _data->getSignal(time);
            if (signal_magnitude > 0) {
                signal_magnitude /=  d;
                signal += signal_magnitude * to_target;
            }

        } else { // Search the children
            for(auto &child : _children) {
                child.second->collectSignal(signal, target, time);
            }
        }
    }
}

void Octree::insert(BoidPtr agent) {
    // If this node doesn't have a data point yet assigned
    // and it is a leaf, then we're done!

    if (isLeafNode()) {
        if (_data != nullptr) {
            uint8_t idx;
            vec3f origin;

            // Insert current agent to child
            getOctantFromPoint(agent->position(), origin, idx);
            _children[idx] = std::make_unique<Octree>(origin, _size * .5f, _level + 1, agent);

            // Remove data from this node and insert into child
            getOctantFromPoint(_data->position(), origin, idx);
            _children[idx] = std::make_unique<Octree>(origin, _size * .5f, _level + 1, std::move(_data));
        } else {
            // Leaf nod is empty
            _data = agent;
        }
    } else {
        uint8_t idx;
        vec3f origin;
        getOctantFromPoint(agent->position(), origin, idx);
        if (_children.find(idx) != _children.end()) {
            _children[idx]->insert(agent);
        } else {
            _children[idx] = std::make_unique<Octree>(origin, _size * .5f, _level + 1, agent);
        }
    }
}

void Octree::update()//int depth=0)
{
//    if (isLeafNode()) {
//        if (_data == NULL) {
//            _aggregated_signal.magnitude = 0;
//            return;
//        }
//
//        _aggregated_signal.center = _data->position();
//        _aggregated_signal.magnitude = _data->value();
//
//        if (_aggregated_signal.magnitude < 0) {
//            _aggregated_signal.magnitude = 0;
//        }
//        return;
//    }
//
//    for(auto &child : _children)
//    {
//        child->update();//depth + 1);
//        _aggregated_signal.center +=
//                child->_aggregated_signal.center * child->_aggregated_signal.magnitude;
//        _aggregated_signal.magnitude +=
//                child->_aggregated_signal.magnitude;
//    }
//    _aggregated_signal.center /= _aggregated_signal.magnitude;
}

