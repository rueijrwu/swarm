#include <glad/glad.h>
#include "gl_world_cube.hpp"
#include "swarm_definition.hpp"

GLworldcube::GLworldcube() : GLobject() {

    _vertices = {
        -1, -1, -1,
         1, -1, -1,
         1,  1, -1,
        -1,  1, -1,
        -1, -1,  1,
         1, -1,  1,
         1,  1,  1,
        -1,  1,  1
    };

    _indices = {0, 1, 2, 3, 4, 5, 6, 7, 0, 4, 1, 5, 2, 6, 3, 7};

    setColor(0, 0, 0);
}

void GLworldcube::render() {
    glBindVertexArray(_vao);
    updateUniform();
    glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)0);
    glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (GLvoid*)(4 * sizeof(uint)));
    glDrawElements(GL_LINES, 8, GL_UNSIGNED_INT, (GLvoid*)(8 * sizeof(uint)));
    glBindVertexArray(0);
}