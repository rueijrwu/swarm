#include "streamer.hpp"

void Streamer::setup() {
    gst_init (nullptr, nullptr);

    _pipeline = gst_pipeline_new (nullptr);
    _source = gst_element_factory_make ("appsrc", nullptr);
    _converter = gst_element_factory_make("videoconvert", nullptr);
    _encoder = gst_element_factory_make("x264enc", nullptr);
    _sink = gst_element_factory_make("filesink", nullptr);

    if (_pipeline == nullptr | _source == nullptr | _converter == nullptr | _encoder == nullptr | _sink == nullptr) {
        return;
    }

    // appsrc cap
    GstCaps* caps_appsrc = gst_caps_new_simple(
            "video/x-raw",
            "format",  G_TYPE_STRING, "RGB",
            "width",  G_TYPE_INT,     _width,
            "height",  G_TYPE_INT,    _height,
            "framerate",  GST_TYPE_FRACTION, 24, 1,
            "interlace-mode", G_TYPE_STRING, "progressive",
            nullptr);
    gst_app_src_set_caps(GST_APP_SRC(_source), caps_appsrc);
    g_object_set(G_OBJECT(_source), "block", true, nullptr);

    // x264
    g_object_set(G_OBJECT(_encoder), "bframes", 0, nullptr);
    g_object_set(G_OBJECT(_encoder), "tune", 4, nullptr);

    // link
    gst_bin_add_many(GST_BIN(_pipeline), _source, _converter, _encoder, _sink, nullptr);
    gst_element_link_many(_source, _converter, _encoder, _sink, nullptr);

    // Set state
    _state = STREAMER_IDLE;

}

void Streamer::setup(int width, int height, std::string filename) {
    _file = filename;
    _width = width;
    _height = height;
    setup();
}

void Streamer::shutdonw() {
    if (_pipeline == nullptr) return;

    // Stop pile line
    gst_object_unref (_pipeline);
}

void Streamer::start() {
    if (_state != STREAMER_IDLE) return;

    // Set file sink
    g_object_set(_sink, "location", _file.c_str(), nullptr);

    // Start pile line
    gst_element_set_state (_pipeline, GST_STATE_PLAYING);

    // Set state
    _state = STREAMER_RUNNING;
}

void Streamer::stop() {
    if (_state < STREAMER_RUNNING) return;

    // push EOS
    gst_app_src_end_of_stream (GST_APP_SRC (_source));

    // Stop pile line
    gst_element_set_state (_pipeline, GST_STATE_NULL);

    // Set state
    _state = STREAMER_IDLE;
}

void Streamer::push() {
    if (_state < STREAMER_RUNNING_ALLOC) return;

    // Push frame to source
    gst_app_src_push_buffer (GST_APP_SRC (_source), _frame);

    // Set state
    _state = STREAMER_RUNNING_PUSH;
}

void Streamer::setSize(uint width, uint height) {
    if (_state != STREAMER_IDLE) return;

    _width = width;
    _height = height;
    _bytes = _width * _height * 3 * sizeof(unsigned char);

    // appsrc cap
    if (_source == nullptr) return;
    GstCaps* caps_appsrc = gst_caps_new_simple(
            "video/x-raw",
            "format",  G_TYPE_STRING, "RGB",
            "width",  G_TYPE_INT,     _width,
            "height",  G_TYPE_INT,    _height,
            "framerate",  GST_TYPE_FRACTION, 30, 1,
            "interlace-mode", G_TYPE_STRING, "progressive",
            nullptr);
    gst_app_src_set_caps(GST_APP_SRC(_source), caps_appsrc);
}

uint8_t* Streamer::newFrame() {
    if (_state < STREAMER_RUNNING) return nullptr;

    // Allocate new frame
    if (_state == STREAMER_RUNNING_ALLOC) gst_buffer_unref(_frame);
    _frame =  gst_buffer_new_and_alloc (_bytes);
    gst_buffer_map (_frame, &_frame_map, GST_MAP_WRITE);
    gst_buffer_unmap (_frame, &_frame_map);

    // Set state
    _state = STREAMER_RUNNING_ALLOC;
    return _frame_map.data;
}