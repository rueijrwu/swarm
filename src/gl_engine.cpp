#include <iostream>
#include <glm/vec4.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <glad/glad.h>

#include "gl_engine.hpp"
#include "glsl.hpp"
#include "swarm_definition.hpp"

void GLAPIENTRY GLmessageCallback(
        GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        GLsizei length,
        const GLchar* message,
        const void* userParam)
{
    fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message);
}

std::shared_ptr<GLengine> MakeGLengine() {
    return std::make_shared<GLengine>();
}

void GLengine::preRender() {
    // Clear
    glClear(GL_COLOR_BUFFER_BIT);
}

void GLengine::postRender() {
//
//    if (_frameIdx % SKIP_FRAMES == 0) {
//        uint8_t * frame = _streamer.newFrame();
//        if (frame != nullptr) {
//            glReadPixels(0, 0, _width, _height, GL_RGB, GL_UNSIGNED_BYTE, frame);
//            _streamer.push();
//        }
//    }
//    _frameIdx++;
}

bool GLengine::setup(int width, int height) {
    _width = width;
    _height = height;

//    glEnable(GL_DEBUG_OUTPUT);
//    glDebugMessageCallback(GLmessageCallback, 0);

    // Build program
    // vertex shader
    int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &VERT_SHADER, nullptr);
    glCompileShader(vertexShader);

    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, nullptr, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        return false;
    }

    // fragment shader
    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &FRAG_SHADER, NULL);
    glCompileShader(fragmentShader);

    // check for shader compile errors
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, nullptr, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
        return false;
    }


    // link shaders
    _program = glCreateProgram();
    glAttachShader(_program, vertexShader);
    glAttachShader(_program, fragmentShader);
    glLinkProgram(_program);

    // check for linking errors
    glGetProgramiv(_program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(_program, 512, nullptr, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
        return false;
    }
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    glUseProgram(_program);

    // Generate projection and view
    glm::mat4 view = glm::lookAt(
            glm::vec3(1000.f, 0.f, 0.f), // Camera is at (4,3,3), in World Space
            glm::vec3(0.f, 0.f, 0.f), // and looks at the origin
            glm::vec3(0.f, 0.f, 1.f)  // Head is up (set to 0,-1,0 to look upside-down)
    );
    glm::mat4 projection = glm::perspective(
            glm::radians(50.f),
            4.f / 3.f,
            0.1f,
            2000.0f);
    _projection_view = projection * view;
    updateViewAndProjection();

    // Resize
    resize(_width, _height);

    // Set background color
    glClearColor(215./255., 239./255, 238./255, 1.f);

    // Generate a unique name
    // Calculate the current time
    time_t CurrentTime = time(nullptr);
    char filename[255];
    strftime(filename, 255, "swarm_%Y%m%d_%H%M%S.avi", localtime(&CurrentTime));
//    _streamer.setup(_width, _height, filename);
//    _streamer.start();

    return true;
}

void GLengine::shutdown() {
    //_streamer.stop();
    //_streamer.shutdonw();
}

void GLengine::resize(int width, int height) {
    glViewport(0, 0, width, height);
}

void GLengine::setCamera() {
}


void GLengine::updateViewAndProjection() {
    glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(_projection_view));
}