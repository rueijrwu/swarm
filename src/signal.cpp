#include "signal.hpp"

Signal::Signal() {
    _transformation.setIdentity();
    _components.setZero();
    //_value = 0;
}

Signal::Signal(const vec3f& v1,
               const vec3f& v2,
               const vec3f& v3) {
    _components.setZero();
    _transformation << v1, v2, v3;
    //_value = 0;
}

Signal& Signal::operator+=(const vec3f &src) {
    _components += _transformation * src;

//    if (ret[0] > 0) {
//        _components[0] += magnitude * ret[0];
//    } else {
//        _components[1] -= magnitude * ret[0];
//    }
//
//    if (ret[1] > 0) {
//        _components[2] += magnitude * ret[1];
//    } else {
//        _components[3] -= magnitude * ret[1];
//    }
//
//    if (ret[2] > 0) {
//        _components[4] += magnitude * ret[2];
//    } else {
//        _components[5] -= magnitude * ret[2];
//    }

    //_value += magnitude;
    return *this;
}

//void Signal::setAxis(const vec3f& v1, const vec3f& v2, const vec3f& v3) {
//    _axis.row(0) = v1;
//    _axis.row(1) = v2;
//    _axis.row(2) = v3;
//}