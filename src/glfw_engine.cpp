#include <memory>
#include "glfw_engine.hpp"

std::shared_ptr<GLFWengine> MakeGLFWengine() {
    return std::make_shared<GLFWengine>();
}