#!/bin/bash
docker build -f DockerfileNvidia \
    --pull\
    --tag swarm \
    . \
    --build-arg user=$USER \
    --build-arg uid=$UID
    
