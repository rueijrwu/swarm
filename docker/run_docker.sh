#!/bin/bash
nvidia-docker run -ti \
    -v /tmp/.docker.xauth:/tmp/.docker.xauth \
    -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
    -v ${HOME}/clion:${HOME}/clion \
    -v ${HOME}/.CLion2019.1:${HOME}/.CLion2019.1 \
    -v ${HOME}/.java/.userPrefs/jetbrains:${HOME}/.java/.userPrefs/jetbrains \
    -v ${HOME}/.java/.userPrefs/prefs.xml:${HOME}/.java/.userPrefs/prefs.xml \
    -v ${HOME}/.config:${HOME}/.config \
    -v ${HOME}/.ccache:${HOME}/.ccache \
    -v ${HOME}/.ideavimrc:${HOME}/.ideavimrc \
    -v ${PWD}/..:${HOME}/swarm \
    -w ${HOME}/swarm \
    -e DISPLAY=$DISPLAY \
    --device=/dev/video0:/dev/video0 \
    --privileged \
    --rm \
    swarm

