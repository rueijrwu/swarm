#!/bin/bash
docker build -f Dockerfile \
    --pull\
    --tag swarm \
    . \
    --build-arg user=$USER \
    --build-arg uid=$UID
